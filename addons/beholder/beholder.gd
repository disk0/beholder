extends Camera

# Mouse or keyboard controls, not both.
export var use_mouse_controls: bool = false
# TODO Implement this.
#   When locked to floor, mouse wheel should move camera, not kinematic.
export var lock_to_floor: bool = false
export var speed: float = 5.0
export var scroll_speed: float = 50.0
export var zoom_scale: float = 2.0
export var slow_scale: float = 4.0
export var enable_collisions: bool = true
export var mount: bool = false
export(PackedScene) var emit_projectile = null
export var emit_force: float = 1000.0
export var max_emit_objects: int = 100
# TODO Implement this.
export var emit_fire_rate: int = 10

# Camera Siblings
var kinematic: KinematicBody = KinematicBody.new()
var collision_shape: CollisionShape = CollisionShape.new()
var spot_light: SpotLight = SpotLight.new()

# Controls Variables
var _is_right_click_pressed: bool = false
var _is_left_click_pressed: bool = false
var _is_zoom_pressed: bool = false
var _is_slow_pressed: bool = false
var _is_left_pressed: bool = false
var _is_right_pressed: bool = false
var _is_forward_pressed: bool = false
var _is_back_pressed: bool = false
var _is_rise_pressed: bool = false
var _is_drop_pressed: bool = false
var _is_emit_pressed: bool = false

# Action Name Variables
var _action_up: String = "beholder_up"
var _action_left: String = "beholder_left"
var _action_down: String = "beholder_down"
var _action_right: String = "beholder_right"
var _action_rise: String = "beholder_rise"
var _action_drop: String = "beholder_drop"
var _action_zoom: String = "beholder_zoom"
var _action_slow: String = "beholder_slow"
var _action_mount: String = "beholder_mount"
var _action_light: String = "beholder_flashlight"
var _action_emit: String = "beholder_emit"
var _emit_objects: Array = []
var _velocity: Vector3 = Vector3()

var _default_map = {
	_action_up: KEY_W,
	_action_left: KEY_A,
	_action_down: KEY_S,
	_action_right: KEY_D,
	_action_rise: KEY_SPACE,
	_action_drop: KEY_Z,
	_action_zoom: KEY_SHIFT,
	_action_slow: KEY_ALT,
	_action_mount: KEY_M,
	_action_light: KEY_F,
	_action_emit: KEY_E
}


func _ready() -> void:
	var global = global_transform.origin
	get_parent().call_deferred("add_child", kinematic)
	kinematic.add_child(spot_light)
	kinematic.add_child(collision_shape)
	spot_light.visible = false
	spot_light.spot_range = 30.0
	collision_shape.shape = BoxShape.new()
	collision_shape.shape.extents = Vector3(0.05, 0.05, 0.05)
	collision_shape.disabled = not enable_collisions
	kinematic.rotation = rotation
	kinematic.translation = translation


func _unhandled_input(event: InputEvent) -> void:
	_is_emit_pressed = _is_pressed(_action_emit)

	if event is InputEventMouseButton:
		var _speed = scroll_speed * 2 if Input.is_key_pressed(KEY_SHIFT) else scroll_speed
		if event.button_index == BUTTON_RIGHT:
			_is_right_click_pressed = event.is_pressed()
		elif event.button_index == BUTTON_LEFT:
			_is_left_click_pressed = event.is_pressed()
		elif event.button_index == BUTTON_WHEEL_UP:
			kinematic.move_and_slide(
				kinematic.global_transform.basis.z * -_speed
			)
		elif event.button_index == BUTTON_WHEEL_DOWN:
			kinematic.move_and_slide(
				kinematic.global_transform.basis.z * _speed
			)

	# Movement inputs.
	if use_mouse_controls:
		_handle_mouse_controls(event)
	else:
		_handle_keyboard_controls(event)

	# Toggled controls.
	if _is_pressed(_action_light):
		spot_light.visible = not spot_light.visible
	if _is_pressed(_action_mount):
		mount = not mount
		set_physics_process(not mount)


func _process(_delta: float) -> void:
	translation = kinematic.translation
	rotation = kinematic.rotation


func _physics_process(delta: float) -> void:
	if not kinematic.is_inside_tree():
		return

	# Handle _is_zoom_pressed/_is_slow_pressed modifiers.
	var speed_multiplier = 1.0
	if _is_zoom_pressed:
		speed_multiplier += zoom_scale
	if _is_slow_pressed:
		speed_multiplier /= slow_scale
	var _speed = speed * speed_multiplier * delta * 60

	# Left and right movement.
	if _is_left_pressed and _is_right_pressed:
		pass
	elif _is_right_pressed:
		kinematic.move_and_slide(kinematic.global_transform.basis.x * _speed)
	elif _is_left_pressed:
		kinematic.move_and_slide(kinematic.global_transform.basis.x * -_speed)
	# Forward and back movement.
	if _is_forward_pressed and _is_back_pressed:
		pass
	elif _is_forward_pressed:
		kinematic.move_and_slide(kinematic.global_transform.basis.z * -_speed)
	elif _is_back_pressed:
		kinematic.move_and_slide(kinematic.global_transform.basis.z * _speed)
	# Up and down movement.
	if _is_rise_pressed:
		kinematic.move_and_slide(Vector3(0.0, speed, 0.0))
	if _is_drop_pressed:
		kinematic.move_and_slide(Vector3(0.0, -speed, 0.0))

	# Object Emissions
	if emit_projectile and _is_emit_pressed:
		var projectile_instance: RigidBody = emit_projectile.instance()
		projectile_instance.global_transform = kinematic.global_transform
		projectile_instance.add_central_force(
			-projectile_instance.transform.basis.z * emit_force
		)
		_emit_objects.append(projectile_instance)
		get_tree().get_root().add_child(projectile_instance)
		# Remove oldest emitions if we are at, or over, the limit.
		if len(_emit_objects) >= max_emit_objects:
			var last = _emit_objects.pop_front()
			last.get_parent().remove_child(last)


func _handle_mouse_controls(event: InputEvent) -> void:
	# Check for mouse motion.
	if _is_left_click_pressed and event is InputEventMouseMotion:
		if Input.is_key_pressed(KEY_SHIFT):
			_drag_position(event)
		else:
			_drag_rotation(event)
	Input.set_mouse_mode(
		Input.MOUSE_MODE_CAPTURED
		if _is_left_click_pressed else
		Input.MOUSE_MODE_VISIBLE
	)


func _handle_keyboard_controls(event: InputEvent) -> void:
	_is_slow_pressed = _is_pressed(_action_slow)
	_is_zoom_pressed = _is_pressed(_action_zoom)
	_is_left_pressed = _is_pressed(_action_left)
	_is_right_pressed = _is_pressed(_action_right)
	_is_forward_pressed = _is_pressed(_action_up)
	_is_back_pressed = _is_pressed(_action_down)
	_is_rise_pressed = _is_pressed(_action_rise)
	_is_drop_pressed = _is_pressed(_action_drop)
	# Check for mouse motion.
	if event is InputEventMouseMotion:
		if _is_right_click_pressed:
			_drag_rotation(event)
	# Check for mouse button.
	elif event is InputEventMouseButton:
		Input.set_mouse_mode(
			Input.MOUSE_MODE_CAPTURED
			if _is_right_click_pressed else
			Input.MOUSE_MODE_VISIBLE
		)


func _look_updown_rotation(rotation: float = 0.0) -> Vector3:
	var ret_val = kinematic.get_rotation() + Vector3(rotation, 0.0, 0.0)
	ret_val.x = clamp(ret_val.x, PI / -2.0, PI / 2.0)
	return ret_val


func _look_leftright_rotation(rotation: float = 0.0) -> Vector3:
	return kinematic.get_rotation() + Vector3(0.0, rotation, 0.0)


func _drag_rotation(event: InputEventMouseMotion) -> void:
	kinematic.set_rotation(_look_leftright_rotation(event.relative.x / -200.0))
	var up_down_rotation = _look_updown_rotation(event.relative.y / -200.0)
	if up_down_rotation.x > -1.5:
		kinematic.set_rotation(_look_updown_rotation(event.relative.y / -200.0))


func _drag_position(event: InputEventMouseMotion) -> void:
	kinematic.move_and_slide(
		kinematic.global_transform.basis.x * -event.relative.x
	)
	kinematic.move_and_slide(
		kinematic.global_transform.basis.y * event.relative.y
	)


func _is_pressed(action: String) -> bool:
	if InputMap.has_action(action):
		return Input.is_action_pressed(action)
	else:
		return Input.is_key_pressed(_default_map[action])
